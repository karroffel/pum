extends Node

signal used_water(charges_left)
signal new_water_charge(charge)

enum Tool {
	NONE,
	SCISSORS,
	WATERING_CAN,
	FERTILIZER,
	HOE,
	SEEDS,
}

var selected_tool = Tool.NONE


# seeds

var seed_bank = preload("res://main/seed_bank.gd").new()
var selected_seed_idx= -1

# water

const WATERING_AMOUNT = 50

const WATER_COOLDOWN_TIME = 2.0
const WATER_MAX_CHARGES = 9

var water_charges = WATER_MAX_CHARGES
var water_cooldown = 0.0


# fertilizer

const FERTILIZER_AMOUNT = 50.0

# TODO
var fertilizer_bottles = 0


func _ready():
	pass

func _process(delta):
	
	# gain water
	
	if water_charges < WATER_MAX_CHARGES:
		water_cooldown -= delta
		
		if water_cooldown < 0:
			water_charges += 1
			water_cooldown = WATER_COOLDOWN_TIME + water_cooldown # cooldown is negative
			
			emit_signal("new_water_charge", water_charges)
			
			if water_charges == WATER_MAX_CHARGES:
				water_cooldown = WATER_COOLDOWN_TIME


func withdraw_water():
	if water_charges <= 0:
		return false
	
	water_charges -= 1
	emit_signal("used_water", water_charges)
	return true


func withdraw_fertilizer():
	# TODO
	return true