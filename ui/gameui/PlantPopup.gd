extends Popup

signal seed_selected()

func _ready():
	pass
	
func show_available_seeds():
	get_node("UIBase/SeedList").set_list(Inventory.seed_bank.seeds)
	show()

func _on_TextureButton_pressed():
	var cur_seed = get_node("UIBase/SeedList").selected_seed
	if(cur_seed != -1):
		Inventory.selected_seed_idx = get_node("UIBase/SeedList").selected_seed
	emit_signal("seed_selected")
	hide()
	
