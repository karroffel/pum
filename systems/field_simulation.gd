extends Object

# "imports"
var Util = preload("res://util.gd")
var PumpkinGrowingSimulation = preload("res://systems/pumpkin_growing_simulation.gd")


signal tile_changed_dry(x, y, tile)
signal tile_changed_moist(x, y, tile)
signal tile_changed_wet(x, y, tile)
signal tile_watered(x, y, tile, amount)

signal tile_ran_out_of_fertilizer(x, y, tile)
signal tile_fertilized(x, y, tile, amount)

signal tile_hoed(x, y, tile)
signal tile_gen_since_hoe_inc(x, y, tile)

signal plant_planted(x, y, tile)
signal plant_harvested(x, y, tile, plant)

signal plant_growth_stopped(x, y, tile)
signal plant_grew_adult(x, y, tile)
signal plant_grew_teen(x, y, tile)





# field properties
var field_dimensions = Vector2()

# field tile properties
var field_tile_default_water_level = 75.0




var default_field_tile_value = {
	"water_level": field_tile_default_water_level,
	"fertilizer_level": 0.0,
	"generations_since_hoe": 0,
	"plant": null
}



var field = null

func _init(field_width, field_height):

	field_dimensions.x = field_width
	field_dimensions.y = field_height

	# construct field
	field = []
	field.resize(field_dimensions.y)

	for i in range(field_dimensions.y):
		field[i] = []
		field[i].resize(field_dimensions.x)

		for j in range(field_dimensions.x):
			field[i][j] = Util.dict_deep_copy(default_field_tile_value)

# One simulation step
# delta is the time since the last step in seconds
# env is a dictionary that describes the enviroment
# 	"light_level":      float from 0 to 1, 0 is complete darkness, 1 is bright daylight
# 	"temperature":      float, temp in celsius
# 	"fertilizer_drain": float, fertilizer drain per tile per second
# 	"water_drain":      float, water drain per tile per second
# 	"rain_per_tile":    float, water gain per tile per second
func simulate(delta, env):
	for y in range(field.size()):
		for x in range(field[y].size()):
			var tile = field[y][x]

			# plant simulation first
			if tile.plant:
				var growth = PumpkinGrowingSimulation.grow(delta, env, x, y, tile)
				
				if growth == 0.0 && tile.plant.last_growth != 0.0:
					emit_signal("plant_growth_stopped", x, y, tile)
				
				
				if tile.plant.growth_progress < 1.5:
					var old_growth_progress = tile.plant.growth_progress
					
					tile.plant.growth_progress += growth
					tile.plant.last_growth = growth
					
					if tile.plant.growth_progress >= 1.5 && old_growth_progress < 1.5:
						emit_signal("plant_grew_adult", x, y, tile)
					if tile.plant.growth_progress >= 1.0 && old_growth_progress < 1.0:
						emit_signal("plant_grew_teen", x, y, tile)

			# field update second
			var old_water_level = tile.water_level
			tile.water_level -= env.water_drain * delta
			tile.water_level += env.rain_per_tile * delta

			if tile.water_level <= 0.0:
				tile.water_level = 0.0

			if tile.water_level <= 0.0 && old_water_level > 0.0:
				emit_signal("tile_changed_dry", x, y, tile)
			elif (tile.water_level <= 50.0 && tile.water_level > 0.0) && (old_water_level > 50.0 || old_water_level <= 0.0):
				emit_signal("tile_changed_moist", x, y, tile)
			elif tile.water_level > 50.0 && old_water_level <= 50.0:
				emit_signal("tile_changed_wet", x, y, tile)
			
			
			var old_fertilzer_level = tile.fertilizer_level
			tile.fertilizer_level -= env.fertilizer_drain * delta
			
			if tile.fertilizer_level <= 0.0:
				tile.fertilizer_level = 0.0
				
				if old_fertilzer_level > 0.0:
					emit_signal("tile_ran_out_of_fertilizer", x, y, tile)


func water(x, y, amount):
	var tile = field[y][x]
	
	var old_water_level = tile.water_level
	tile.water_level += amount
	
	emit_signal("tile_watered", x, y, tile, amount)
	
	# This lets us use negative amounts, maybe debuffs?
	if tile.water_level <= 0.0 && old_water_level > 0.0:
		emit_signal("tile_changed_dry", x, y, tile)
	elif (tile.water_level <= 50.0 && tile.water_level > 0.0) && (old_water_level > 50.0 || old_water_level <= 0.0):
		emit_signal("tile_changed_moist", x, y, tile)
	elif tile.water_level > 50.0 && old_water_level <= 50.0:
		emit_signal("tile_changed_wet", x, y, tile)
	

func fertilize(x, y, amount):
	var tile = field[y][x]
	
	var old_fertilizer_level = tile.fertilizer_level
	tile.fertilizer_level += amount
	
	emit_signal("tile_fertilized", x, y, tile, amount)

func hoe_tile(x, y):
	var tile = field[y][x]

	if tile.plant:
		return false

	var tile_props = Util.dict_deep_copy(tile)

	tile.generations_since_hoe = 0

	emit_signal("tile_hoed", x, y, tile_props)
	return true



# plant a plant at x,y in the field.
# "plant" is a dictionary containing the data about the plant.
# "plant" is then owned by this function as it doesn't perform copying.
func plant(x, y, plant):
	var tile = field[y][x]

	if tile.plant:
		return false

	var tile_props = Util.dict_deep_copy(tile)
	tile.plant = plant

	emit_signal("plant_planted", x, y, tile_props)
	return true



func harvest(x, y):
	var tile = field[y][x]
	
	if tile.plant:
		var plant = tile.plant
		
		tile.plant = null
		
		emit_signal("plant_harvested", x, y, tile, plant)
		
		tile.generations_since_hoe += 1
		
		emit_signal("tile_gen_since_hoe_inc", x, y, tile)
		
		return true
	
	return false