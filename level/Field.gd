extends Node2D

var Util = preload("res://util.gd")
var FieldSimulation = preload("res://systems/field_simulation.gd")
var PumpkinGrowingSimulation = preload("res://systems/pumpkin_growing_simulation.gd")


var field_size = Vector2(4, 4)
var field = null



onready var tiles = $Tiles
onready var plants = $Plants
onready var effects = $Effects

func _ready():
	
	var example_seed = {
		"growth_time": 10.0,
		"properties": {
			"dimension":1,
			"number":2
		}
	}
	
	Inventory.seed_bank.add_seed(example_seed)
	
	field = FieldSimulation.new(field_size.x, field_size.y)
	
	field.connect("plant_planted", self, "_on_plant_planted")
	field.connect("plant_harvested", self, "_on_plant_harvested_display")
	field.connect("plant_harvested", self, "_on_plant_harvested_seeds")
	field.connect("plant_grew_teen", self, "_on_plant_grew_teen")
	field.connect("plant_grew_adult", self, "_on_plant_grew_adult")
	
	field.connect("tile_changed_dry", self, "_on_tile_changed_dry")
	field.connect("tile_changed_moist", self, "_on_tile_changed_moist")
	field.connect("tile_changed_wet", self, "_on_tile_changed_wet")
	
	field.connect("tile_fertilized", self, "_on_tile_fertilized")
	field.connect("tile_ran_out_of_fertilizer", self, "_on_tile_ran_out_of_fertilizer")
	
	field.connect("tile_hoed", self, "_on_tile_hoed")
	field.connect("tile_gen_since_hoe_inc", self, "_on_tile_gen_since_hoe_inc")
	


# TODO this was a quick and dirty implementation to see if it all works
# And it does, wheeeey \o/

func _input(event):
	if event is InputEventScreenTouch:
		if event.pressed && not event.is_echo():
			var pos = tiles.to_local(event.position)
			var tile_pos = tiles.world_to_map(pos)
			var field_pos = tile_pos - Vector2(3, -2)
			
			if field_pos.x >= 0 && field_pos.x < 4 && field_pos.y >= 0 && field_pos.y < 4:
				
				clicked_on_field(field_pos.x, field_pos.y)

	
	# if event is InputEventScreenTouch:
	#	if event.pressed && not event.is_echo():
	#		print("Buh")

	# if event is InputEventScreenDrag:
	#	print("draaag")

func _process(delta):
	var env = {
		"fertilizer_drain": 1.0,
		"water_drain": 1.0,
		"rain_per_tile": 0.0,
		
		"light_level": 1.0,
		"temperature": 15.0,
	}
	field.simulate(delta, env)


func clicked_on_field(x, y):
	match Inventory.selected_tool:
		Inventory.NONE:
			pass
		
		Inventory.SCISSORS:
			field.harvest(x, y)
			
		Inventory.WATERING_CAN:
			if Inventory.withdraw_water():
				field.water(x, y, Inventory.WATERING_AMOUNT)
			
		Inventory.FERTILIZER:
			if Inventory.withdraw_fertilizer():
				field.fertilize(x, y, Inventory.FERTILIZER_AMOUNT)
			
		Inventory.HOE:
			field.hoe_tile(x, y)
			
		Inventory.SEEDS:
			
			var the_seed = Inventory.seed_bank.use_seed(Inventory.selected_seed_idx)
			
			if the_seed != null:
				
				var pumpkin = PumpkinGrowingSimulation.make_pumpkin_from_seed(the_seed)
				
				field.plant(x, y, pumpkin)

func _on_plant_planted(x, y, tile):
	var offset = Vector2(0, -64)
	
	var pos = tiles.map_to_world(Vector2(x, y) + Vector2(3, -2))
	
	var sprite = Sprite.new()
	sprite.texture = load("res://assets/tilesets/basic/pumpkins/plant_2_stage_0.png")
	
	sprite.set_name(str(x) + "," + str(y))
	
	plants.add_child(sprite)
	sprite.position = pos + offset

func _on_plant_harvested_display(x, y, tile, plant):
	
	var the_plant = plants.get_node(str(x) + "," + str(y))
	
	if the_plant != null:
		the_plant.queue_free()

func _on_plant_harvested_seeds(x, y, tile, plant):
	
	var seeds = PumpkinGrowingSimulation.create_seeds_from_pumpkin(plant)
	
	for seed_ in seeds:
		Inventory.seed_bank.add_seed(seed_)

func _on_plant_grew_teen(x, y, tile):
	
	var sprite = plants.get_node(str(x) + "," + str(y))
	
	sprite.texture = load("res://assets/tilesets/basic/pumpkins/plant_2_stage_1.png")

func _on_plant_grew_adult(x, y, tile):
	
	var sprite = plants.get_node(str(x) + "," + str(y))
	
	sprite.texture = load("res://assets/tilesets/basic/pumpkins/plant_2_stage_2.png")



# TODO those are just for demo purposes, remake all of this but a lot nicer

func _on_tile_changed_dry(x, y, tile):
	var tile_pos = Vector2(x, y) + Vector2(3, -2)
	
	var tile_num = 0 + (tiles.get_cell(tile_pos.x, tile_pos.y) / 3 * 3)
	
	tiles.set_cell(tile_pos.x, tile_pos.y, tile_num)



func _on_tile_changed_moist(x, y, tile):
	var tile_pos = Vector2(x, y) + Vector2(3, -2)
	
	var tile_num = 1 + (tiles.get_cell(tile_pos.x, tile_pos.y) / 3 * 3)
	
	tiles.set_cell(tile_pos.x, tile_pos.y, tile_num)



func _on_tile_changed_wet(x, y, tile):
	var tile_pos = Vector2(x, y) + Vector2(3, -2)
	
	var tile_num = 2 + (tiles.get_cell(tile_pos.x, tile_pos.y) / 3 * 3)
	
	tiles.set_cell(tile_pos.x, tile_pos.y, tile_num)



func _on_tile_fertilized(x, y, tile, amount):
	
	var offset = Vector2(0, 128)
	
	var pos = tiles.map_to_world(Vector2(x, y) + Vector2(3, -2))
	
	var particles = preload("res://level/FertilizerEffect.tscn").instance()
	
	particles.set_name(str(x) + "," + str(y))
	
	effects.add_child(particles)
	particles.position = pos + offset

func _on_tile_ran_out_of_fertilizer(x, y, tile):
	
	print("run out of fertilizer")
	
	var effect = effects.get_node(str(x) + "," + str(y))
	
	effect.queue_free()



func _on_tile_hoed(x, y, tile):
	
	var tile_pos = Vector2(x, y) + Vector2(3, -2)
	
	var tile_cell = tiles.get_cell(tile_pos.x, tile_pos.y)
	var tile_wetness = tile_cell % 3
	
	var new_tile_cell = 2 * 3 + tile_wetness
	
	tiles.set_cell(tile_pos.x, tile_pos.y, new_tile_cell)


func _on_tile_gen_since_hoe_inc(x, y, tile):
	
	var tile_pos = Vector2(x, y) + Vector2(3, -2)
	
	var tile_cell = tiles.get_cell(tile_pos.x, tile_pos.y)
	var tile_wetness = tile_cell % 3
	
	if tile.generations_since_hoe == 3:
		var new_tile_cell = 1 * 3 + tile_wetness
		
		tiles.set_cell(tile_pos.x, tile_pos.y, new_tile_cell)
	
	if tile.generations_since_hoe == 6:
		var new_tile_cell = 0 * 3 + tile_wetness
		
		tiles.set_cell(tile_pos.x, tile_pos.y, new_tile_cell)
