extends TextureButton

export var my_tool = 0

func _ready():
	connect("toggled", self, "_on_button_pressed")
	pass

func _on_button_pressed(press):
	if(press):
		for  c in get_parent().get_children():
			if(c!=self):
				c.pressed = false
		Inventory.selected_tool = my_tool
	else:
		Inventory.selected_tool = Inventory.Tool.NONE