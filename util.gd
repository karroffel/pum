extends Object

# This is only a partially deep copy.
# If a key inside the dictionary is not a primitive (array, dict)
# then that key will only be copied shallow
static func dict_deep_copy(dict):
	var new_dict = {}
	
	for key in dict:
		var value = dict[key]
		
		match typeof(value):
			TYPE_ARRAY:      value = array_deep_copy(value)
			TYPE_DICTIONARY: value = dict_deep_copy(value)
		
		new_dict[key] = value
	
	return new_dict

static func array_deep_copy(arr):
	var new_arr = []
	new_arr.resize(arr.size())
	
	for i in range(arr.size()):
		var value = arr[i]
		
		match typeof(value):
			TYPE_ARRAY:      value = array_deep_copy(value)
			TYPE_DICTIONARY: value = dict_deep_copy(value)
		
		new_arr[i] = value
	
	return new_arr