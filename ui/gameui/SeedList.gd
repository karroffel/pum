extends Container

var seed_info = preload("SeedInfo.tscn")
var selected_seed 

func _ready():
	
	pass

func set_list(seed_list):
	var container = get_node("ScrollContainer/VBoxContainer")
	for c in container.get_children():
		container.remove_child( c )
		c.queue_free()
	for i in range( seed_list.size() ):
		var child = seed_info.instance()
		child.set_seed_infos(i, seed_list[i])
		container.add_child(child)
		child.connect("selected_seed",self, "selected_seed")
		child.connect("unselected", self, "unselected")

func selected_seed(which, who):
	selected_seed = which
	for c in get_node("ScrollContainer/VBoxContainer").get_children():
		if( c == who ):
			continue
		c.unselect()
	
func unselected():
	selected_seed = -1