extends Object


static func grow(delta, env, x, y, tile):
	
	var grow_factor = 1.0
	
	if tile.water_level == 0.0:
		return 0.0
	
	# take generations into account
	grow_factor = 1 / (max(tile.generations_since_hoe - 2, 0) + 1)
	
	# take light level into account
	grow_factor *= env.light_level
	
	# take temperature into account
	grow_factor *= 1 # TODO
	
	grow_factor += tile.fertilizer_level
	
	return grow_factor * delta / tile.plant.growth_time



static func make_pumpkin_from_seed(seed_):
	
	var Util = preload("res://util.gd")
	
	var pumpkin = Util.dict_deep_copy(seed_)
	
	pumpkin.growth_progress = 0.0
	pumpkin.last_growth = 0.0
	
	# TODO
	
	return pumpkin


static func create_seeds_from_pumpkin(pumpkin):
	
	var Util = preload("res://util.gd")
	
	# TODO
	
	var seeds = []
	

	
	var one_seed = Util.dict_deep_copy(pumpkin)
	one_seed.erase("growth_progress")
	one_seed.erase("last_growth")
	
	seeds.append(Util.dict_deep_copy(one_seed))
	seeds.append(Util.dict_deep_copy(one_seed))
	
	return seeds