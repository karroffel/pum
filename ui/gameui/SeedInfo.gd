extends NinePatchRect

var id
signal selected_seed(which, who)
signal unselected()

func set_seed_infos(index,_seed):
	get_node("Dimension").set_text(str(_seed.properties.dimension))
	get_node("Growth").set_text(str(_seed.growth_time))
	get_node("Number").set_text(str(_seed.properties.number))
	id = index

func _on_TextureButton_toggled( pressed ):
	if(pressed):
		emit_signal("selected_seed", id, self)
		print("selected seed" + str(id))
	else:
		emit_signal("unselected")
		
func unselect():
	#TODO change patch 9 texture
	get_node("TextureButton").pressed = false
