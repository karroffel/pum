extends Control

func _ready():
	Inventory.seed_bank.connect("seed_used", self, "seed_used")
	
func seed_used(bla, blah):
	$ControlButtons/SeedsButton.pressed = false

func _on_SeedsButton_toggled( pressed ):
	$SeedSelectionPopup.show_available_seeds()
