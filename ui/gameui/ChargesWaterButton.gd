extends 'res://ui/gameui/GameToolButton.gd'

onready var label = get_node("Progress/Number")
onready var progrees = get_node("Progress")

func _ready():
	_set_water_charge(Inventory.water_charges)
	Inventory.connect("new_water_charge", self, "_set_water_charge")
	Inventory.connect("used_water", self, "_set_water_charge")
	pass

func _set_water_charge(amount):
	print("new charge")
	label.set_text( str(amount) )

func _process(delta):
	var cd = 1 -  (Inventory.water_cooldown) / Inventory.WATER_COOLDOWN_TIME
	#print(str(cd))
	progrees.value = cd