extends Object

var Util = preload("res://util.gd")

signal seed_list_changed()

signal seed_added(seed_, idx)
signal seed_used(seed_, idx)

signal seed_not_available(idx)

# 
#
#
#
#

var seeds = []

func add_seed(seed_):
	var new_seed = Util.dict_deep_copy(seed_)
	seeds.append(new_seed)
	
	emit_signal("seed_added", new_seed, seeds.size())
	emit_signal("seed_list_changed")



func use_seed(idx):
	if idx >= 0 && idx < seeds.size():
		var the_seed = seeds[idx]
		seeds.remove(idx)
	
	
		emit_signal("seed_used", the_seed, idx)
		emit_signal("seed_list_changed")
	
		return the_seed
	else:
		emit_signal("seed_not_available", idx)
		return null